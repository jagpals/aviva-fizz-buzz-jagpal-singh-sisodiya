﻿// <copyright file="DivisibleByThreeRule.cs" company="Test"> 
// 2017</copyright> 
// <summary> 
// Divisible by 3 Rule
// </summary> 
namespace FizzBuzzRule.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzRule.Interfaces;

    /// <summary>
    /// Rule to check divisibility of 3
    /// </summary>
    public class DivisibleByThreeRule : IDivisibleRule
    {
        /// <summary>
        /// Rule result
        /// </summary>
        private string result = string.Empty;

        /// <summary>
        /// Gets the result of rule
        /// </summary>
        public string Result
        {
            get { return this.result; }
        }

        /// <summary>
        /// To check rule divisibility conditions by 3 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        public bool Run(int input, DayOfWeek dayOfTheWeek)
        {
            if (input % 3 == 0)
            {
                this.result = dayOfTheWeek == DayOfWeek.Wednesday ? Constants.WIZZ : Constants.FIZZ;
                return true;
            }

            return false;
        }
    }
}
