﻿// <copyright file="IFizzBuzzService.cs" company="Test"> 
// 2017</copyright> 
// <summary> 
// Fizz Buzz Service Interface
// </summary> 
namespace FizzBuzzServices.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for fizz buzz service
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        ///  Method to get fizz buzz list
        /// </summary>
        /// <param name="maxLimit">input number</param>
        /// <returns>Fizz buzz list</returns>
        IEnumerable<string> GetFizzBuzzList(int maxLimit);
    }
}
