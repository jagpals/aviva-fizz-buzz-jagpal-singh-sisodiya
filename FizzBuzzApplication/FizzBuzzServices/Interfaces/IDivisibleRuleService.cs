﻿// <copyright file="IDivisibleRuleService.cs" company="Test"> 
// 2017</copyright> 
// <summary> 
// Divisible Rule Service interface
// </summary> 
namespace FizzBuzzServices.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzRule.Interfaces;

    /// <summary>
    /// Interface of DivisibleRuleService
    /// </summary>
    public interface IDivisibleRuleService
    {
        /// <summary>
        /// Function to get all rules which returned true for the given input
        /// </summary>
        /// <param name="number">input number</param>
        /// <returns>Enumerable of rules which returned true for the given input</returns>
        IEnumerable<IDivisibleRule> GetMatchedRules(int number);
    }
}
